#!/usr/bin/env python
# encoding: utf-8
"""
Created by Lynda Lavoie on 2010-11-18.
"""
###
# Excellent!
# 15/15
# Si tu as ce message d'erreur, il faut rouler la patch que je vous avais envoyée (pyo_patch.py)
# "global FORMATS not defined"
###

import wx, os

from pyo import *

from mylibrairie2 import BM1

s = Server().boot()
s.amp = 0.5

 
Player1 = BM1(input="reyong.wav",accent=1,dur=1,mul=1).out()
Player2 = BM1(input="reyong1.wav",accent=1,dur=1,mul=1).out()
Player3 = BM1(input="reyong2.wav",accent=1,dur=1,mul=1).out()
Player4 = BM1(input="reyong4.wav",accent=1,dur=1,mul=1).out()


#ameliorations possibles : 
# 
#beat 4/4 - 2 vs 3 - ca change une fois mais ca rechange plus ? 
#éventuellement faire du 3/4 ... 

#Button RECORD ... marche pas .... "global FORMATS not defined" ?? 
#dans "Accent" random beat marche pas ....       
#faire un choix FX 'Rien' ... 


class PlayerPanel(wx.Panel):
    def __init__(self, parent,title,pos,size,audio):
        wx.Panel.__init__(self,parent,id=-1,pos=pos,size=size,style=wx.SUNKEN_BORDER)
        
        self.audio = audio 
        
        self.SetBackgroundColour("#006600")
                
        
        #----------------Choix du son   
        
        self.choixson = wx.StaticText(self, id=-1, label="path:", pos=(100,10), size=wx.DefaultSize)
        self.choixson.SetForegroundColour("#33CC33")
        self.choixson.SetBackgroundColour("#003300")
        
           
        self.chooseButton = wx.Button(self, id=-1, label="SON", pos=(100,30))
        self.chooseButton.Bind(wx.EVT_BUTTON, self.loadSnd)
        
        #----------------Choix Effet
               
        fxchx = ['Delay', 'Disto', 'Degrade','Filtre', 'Chorus']
        
        self.fx1Text = wx.StaticText(self, id=-1, label="FX", pos=(100,60), size=wx.DefaultSize)
        self.fx1Text.SetForegroundColour("#33CC33")
        self.fx1Text.SetBackgroundColour("#003300")
        self.fx1= wx.Choice(self, id=-1, pos=(100,80), size=wx.DefaultSize, choices=fxchx)
        self.fx1.Bind(wx.EVT_CHOICE, self.FX)
        
        self.choixeffet = self.fx1.GetStringSelection()
        
        
        
        #----------------Sliders Parametres effet
        
        self.par1Text = wx.StaticText(self, id=-1, label= " PARAMETRE 1: 1.0", pos=(100,100), size=wx.DefaultSize)
        self.par1 = wx.Slider(self, id=-1, value= 1, minValue=1, maxValue=10, pos=(100,120), size=wx.DefaultSize)
        self.par1Text.SetForegroundColour("#33CC33")
        self.par1Text.SetBackgroundColour("#003300")
        
        self.par1.Bind(wx.EVT_SLIDER, self.ParametreP1)
        
        self.par2Text = wx.StaticText(self, id=-1, label= " PARAMETRE 2: 1.0", pos=(100,150), size=wx.DefaultSize)
        self.par2 = wx.Slider(self, id=-1, value= 1, minValue=1, maxValue=10, pos=(100,170), size=wx.DefaultSize)
        self.par2Text.SetForegroundColour("#33CC33")
        self.par2Text.SetBackgroundColour("#003300")
        
        self.par2.Bind(wx.EVT_SLIDER, self.ParametreP2)
        
        #-----------------Accents sur les temps forts ou faibles 
        
        acc = ["on beat","up beat","in between","random beat"]

        self.accent1Text = wx.StaticText(self, id=-1, label=" ACCENT", pos=(100,200), size=wx.DefaultSize)
        self.accent1 = wx.Choice(self, id=-1, pos=(100,220), size=wx.DefaultSize, choices=acc)
        self.accent1Text.SetForegroundColour("#33CC33")
        self.accent1Text.SetBackgroundColour("#003300")
        
        self.accent1.Bind(wx.EVT_CHOICE, self.Accent)
        
        #-----------------Vitesse lecture du son 
        
        self.vitText = wx.StaticText(self,id=-1, label="VITESSE DE LECTURE : 1.0", pos=(100,250), size=wx.DefaultSize)
        self.vit = wx.Slider(self, id=-1,value=1000, minValue=100, maxValue=5000, pos=(100,270), size=wx.DefaultSize)
        self.vitText.SetForegroundColour("#33CC33")
        self.vitText.SetBackgroundColour("#003300")
        
        self.vit.Bind(wx.EVT_SLIDER, self.changeVitesse)
        
        #-----------------Volume du son
        
        self.volplayText = wx.StaticText(self,id=-1, label="VOLUME: 1.0", pos=(100,300), size=wx.DefaultSize)
        self.volplay = wx.Slider(self, id=-1,value=1000, minValue=0, maxValue=5000, pos=(100,320), size=wx.DefaultSize)
        self.volplayText.SetForegroundColour("#33CC33")
        self.volplayText.SetBackgroundColour("#003300")
        
        self.volplay.Bind(wx.EVT_SLIDER, self.VolPlayer)
        #-----------------Sizer 
        
        
        Sizer = wx.BoxSizer(wx.VERTICAL)
        Sizer.Add(self.choixson, 0, wx.ALIGN_LEFT|wx.ALL, 5)
        Sizer.Add(self.chooseButton, 0, wx.ALIGN_CENTER|wx.ALL, 5)
        Sizer.Add(self.fx1Text, 0, wx.ALIGN_CENTER|wx.ALL, 5)
        Sizer.Add(self.fx1, 0, wx.ALIGN_CENTER|wx.ALL, 5)
        Sizer.Add(self.par1Text, 0, wx.ALIGN_CENTER|wx.ALL, 5)
        Sizer.Add(self.par1, 0, wx.ALIGN_CENTER|wx.ALL, 5)
        Sizer.Add(self.par2Text, 0, wx.ALIGN_CENTER|wx.ALL, 5)
        Sizer.Add(self.par2, 0, wx.ALIGN_CENTER|wx.ALL, 5)
        Sizer.Add(self.accent1Text, 0, wx.ALIGN_CENTER|wx.ALL, 5)
        Sizer.Add(self.accent1, 0, wx.ALIGN_CENTER|wx.ALL, 5)
        Sizer.Add(self.vitText, 0, wx.ALIGN_CENTER|wx.ALL, 5)
        Sizer.Add(self.vit, 0, wx.ALIGN_CENTER|wx.ALL, 5)
        Sizer.Add(self.volplayText, 0, wx.ALIGN_CENTER|wx.ALL, 5)
        Sizer.Add(self.volplay, 0, wx.ALIGN_CENTER|wx.ALL, 5)
        
  
        self.SetSizerAndFit(Sizer)
        
        
        
        
            
    def loadSnd(self, evt):
        dlg = wx.FileDialog(self, message="Nouveau son...", style=wx.FD_OPEN)
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            if path != "":
                x = self.vit.GetValue() * 0.001
                self.audio.setInput(path)
                self.audio.setDur(x)  
                nom = path[-20:]
                self.choixson.SetLabel(" %s" % nom)
                    
        dlg.Destroy()

        
         
        
    def FX(self, evt):
        self.audio.setFx(evt.GetString())   


    def ParametreP1(self,evt):

        self.choixeffet = self.fx1.GetStringSelection()
         
        x = evt.GetInt()

        if self.choixeffet =='Delay':
            a = x * 0.1
            self.par1Text.SetLabel("delay: %.1f" % a)
            self.audio.setP1(a)

        if self.choixeffet == 'Filtre':
            b = x * 500
            self.par1Text.SetLabel("filtre freq: %.1f" % b)
            b = x * 500
            self.audio.setP1(b)

        if self.choixeffet == 'Disto':
            c = x * 0.1
            self.par1Text.SetLabel("drive: %.1f" % c)
            self.audio.setP1(c)            

        if self.choixeffet== 'Chorus':
            if x <= 5:
                d = x 
            else :
                d = 5
            self.par1Text.SetLabel("depth: %.1f" % d)
            self.audio.setP1(d)

        if self.choixeffet== 'Degrade':
            e = x * 3
            self.par1Text.SetLabel("bitdepth: %.1f" % e)
            self.audio.setP1(e)  

        #elif self.choixeffet == 'Rien':
             #self.par1Text.SetLabel("pas effet")
            
            
            
    def ParametreP2(self,evt):

        x = evt.GetInt()

        if self.choixeffet =='Delay':
            aa = x * 0.1
            self.par2Text.SetLabel("feedback: %.1f" % aa)
            self.audio.setP1(aa)

        if self.choixeffet== 'Filtre':
            bb =  x * 5
            self.par2Text.SetLabel("filtre q: %.1f" % bb)
            self.audio.setP2(bb)

        if self.choixeffet== 'Disto':
            cc = x * 0.1 
            self.par2Text.SetLabel("slope: %.1f" % cc)
            self.audio.setP2(cc)           

        if self.choixeffet== 'Chorus':   
            dd =  x * 0.1
            self.par2Text.SetLabel("feedback: %.1f" % dd)
            self.audio.setP2(dd)

        if self.choixeffet== 'Degrade':
            ee =  x * 0.1 
            self.par2Text.SetLabel("srscale: %.1f" % ee)
            self.audio.setP2(ee)

        #elif self.choixeffet== 'Rien':
            #self.par2Text.SetLabel("pas effet")
   
    def Accent(self,evt):

        if evt.GetString() == "on beat":
            self.audio.setAccent(1)    
        if evt.GetString() == "up beat":
            self.audio.setAccent(2)
        if evt.GetString() == "in between":
            self.audio.setAccent(3)
        if evt.GetString() == "random beat":
            self.audio.setAccent(0)
            

    def setSound(self, evt):
        x = self.vit.GetValue() * 0.001
        self.audio.setInput(evt.GetString())
        self.audio.setDur(x)


    def changeVitesse(self, evt):
        y = evt.GetInt() * 0.001
        self.vitText.SetLabel("Vitesse de lecture : %.1f" % y)
        self.audio.setDur(y)

    def VolPlayer(self,evt):
        xx = self.volplay.GetValue() * 0.001
        self.volplayText.SetLabel(" Volume : %.1f" % xx)
        self.audio.setMul(xx)
      
        
 ##################################       
        
             
class MyFrame(wx.Frame):
    
    def __init__(self, parent, title, pos, size, audio1, audio2, audio3, audio4):
        wx.Frame.__init__(self, parent, id=-1, title=title, pos=pos, size=size)
        
        self.panel = wx.Panel(self)
        
        self.audio1 = audio1
        self.audio2 = audio2
        self.audio3 = audio3
        self.audio4 = audio4
        
        self.deuxTrois = 0
       
        
        self.panel.SetBackgroundColour("#000000")

        self.onOffText = wx.StaticText(self.panel, id=-1, label="GENERAL CONTROL", pos=(20,10), size=wx.DefaultSize)
        self.onOffText.SetForegroundColour('red')
        self.onOff = wx.ToggleButton(self.panel, id=-1, label="Push Me", pos=(20,30), size=wx.DefaultSize)
        self.onOff.SetForegroundColour('red')
        self.onOff.Bind(wx.EVT_TOGGLEBUTTON, self.handleAudio)
        
        #--------------------- Tempo 
        
        self.tempText = wx.StaticText(self.panel, id=-1, label= " BPM : 60", pos=(20,70), size=wx.DefaultSize)
        self.tempText.SetForegroundColour('red')
        self.temp = wx.Slider(self.panel, id=-1, value=60, minValue=30, maxValue=250, pos=(15,90), size=wx.DefaultSize)
        self.temp.Bind(wx.EVT_SLIDER, self.Tempo)
        
        #--------------------- Option deux contre trois 
        
        self.dctText = wx.StaticText(self.panel, id=-1, label="BEAT", pos=(20,140), size=wx.DefaultSize)
        self.dctText.SetForegroundColour('red')
        
        self.dct = wx.ToggleButton(self.panel, id=-1, label="4/4" , pos=(20,160), size=wx.DefaultSize)
        self.dct.Bind(wx.EVT_TOGGLEBUTTON, self.Deuxcontretrois)
        
        
        #--------------------- Volume
        
        self.volText = wx.StaticText(self.panel, id=-1, label= " VOLUME MAIN: 1.0", pos=(20,210), size=wx.DefaultSize)
        self.volText.SetForegroundColour('red')
        self.vol = wx.Slider(self.panel, id=-1, value= 100, minValue=0, maxValue=2000, pos=(15,230), size=wx.DefaultSize)
        self.vol.Bind(wx.EVT_SLIDER, self.Volume)
        
        #---------------------Panneaux secondaires 
        
        self.Title1Text = wx.StaticText(self.panel, id=-1, label="BEAT1", pos=(240,30), size=wx.DefaultSize)
        self.Title1Text.SetForegroundColour('red')
        self.playerpanel1 = PlayerPanel(self.panel, title = 'BEAT1', pos=(175,50),size=(200,400),audio=self.audio1)
        
        
        self.Title2Text = wx.StaticText(self.panel, id=-1, label="BEAT2", pos=(465,30), size=wx.DefaultSize)
        self.Title2Text.SetForegroundColour('red')
        self.playerpanel2 = PlayerPanel(self.panel, title='BEAT2', pos=(400,50),size=(200,400),audio=self.audio2)
        
        
        self.Title3Text = wx.StaticText(self.panel, id=-1, label="BEAT3", pos=(690,30), size=wx.DefaultSize)
        self.Title3Text.SetForegroundColour('red')
        self.playerpanel3 = PlayerPanel(self.panel, title= 'BEAT3', pos=(625,50),size=(200,400),audio=self.audio3)
        
        
        self.Title4Text = wx.StaticText(self.panel, id=-1, label="BEAT4", pos=(915,30), size=wx.DefaultSize)
        self.Title4Text.SetForegroundColour('red')
        self.playerpanel4 = PlayerPanel(self.panel, title = 'BEAT4', pos=(850,50),size=(200,400),audio=self.audio4)
        
        
        self.recText = wx.StaticText(self.panel, id=-1, label="RECORD", pos=(20,260))
        self.recText.SetForegroundColour('red')
        self.rec = wx.ToggleButton(self.panel, id=-1, label="start / stop", pos=(20,280), size=(95,20))
        self.rec.Bind(wx.EVT_TOGGLEBUTTON, self.handleRec)
        
        
        
        #---------------------Roar button! 
        
        self.roarplayer = SfPlayer(path='wildcat.wav',speed=1,loop=False,mul=.3)
        
        self.roarText = wx.StaticText(self.panel, id=-1, label= "! R  ø  a  R !", pos=(35,330), size=wx.DefaultSize)
        self.roarText.SetForegroundColour('red')        
        
        self.roar = wx.Bitmap('LB09.png')
        wx.EVT_PAINT(self, self.OnPaint)
        self.roarbutton = wx.BitmapButton(self.panel, 30, self.roar, (45,350))
        self.roarbutton.Bind(wx.EVT_BUTTON, self.OnClick)




    def OnClick(self,event):
        self.roarplayer.out()



    def OnPaint(self, event):
        dc = wx.PaintDC(self)
        dc.DrawBitmap(self.roar, 50, 50)
      
         
              
        
    def handleAudio(self, evt):
        if evt.GetInt() == 1:
            s.start()
            self.onOff.SetLabel("Joue")
            
        else:
            s.stop() 
            self.onOff.SetLabel("Joue Pu")
                   
        
    
   
    def Deuxcontretrois(self, evt):
        
        self.deuxTrois = evt.GetInt()
        if evt.GetInt() == 1:
            fac1 = 15.0
            fac2 = 20.0            
            self.audio1.setTaps(16)
            self.audio2.setTaps(12)
            self.audio3.setTaps(16)
            self.audio4.setTaps(12)
            self.dct.SetLabel("2 vs 3") 
         
        else:
            fac1 = fac2 = 15.0    
            self.audio1.setTaps(16)
            self.audio2.setTaps(16)
            self.audio3.setTaps(16)
            self.audio4.setTaps(16)
            
            self.dct.SetLabel("4/4") 
            
        z = self.temp.GetValue()
        
        self.audio1.setTime( fac1 / z)
        self.audio2.setTime( fac2 / z)
        self.audio3.setTime( fac1 / z)
        self.audio4.setTime( fac2 / z)
            
            
    def Tempo(self, evt):
        if self.deuxTrois == 1:
            fac1 = 15.0
            fac2 = 20.0
        else:
            fac1 = fac2 = 15.0    
            
        z = self.temp.GetValue()
        self.tempText.SetLabel("BPM : %.1f" % z)
        
        self.audio1.setTime( fac1 / z)
        self.audio2.setTime( fac2 / z)
        self.audio3.setTime( fac1 / z)
        self.audio4.setTime( fac2 / z)

       ## 15 / BMP = time ... (en 4/4) 
       # 20 / BPM = time ( 60/3) en tertiaire ...  

         
    def Volume(self,evt):
        xx = self.vol.GetValue() * 0.001
        self.volText.SetLabel(" VOLUME MAIN: %.1f" % xx)
        s.amp =  xx   
        
    def handleRec(self, evt):
        if evt.GetInt() == 1:
            ext = "wav"
            path = os.path.join(os.path.expanduser("~"), "Desktop", "BeatMachine.%s" % ext)
            s.recordOptions(dur=-1, filename=path, fileformat=0, sampletype= 0)
            s.recstart()
            self.onOff.SetLabel("Record")
            
        else:
            s.recstop() 
            self.onOff.SetLabel("Stop")
               
        
        
        
        
app = wx.PySimpleApp()


mainFrame = MyFrame(None, title= 'DIY BeatMachine', pos=(100,100), size=(1075,500),audio1 = Player1, audio2= Player2, audio3 = Player3, audio4= Player4)


mainFrame.Show()


app.MainLoop()

