class HostenPawnSoundGroup extends UTPawnSoundGroup; 

defaultproperties

{


DefaultJumpingSound=SoundCue'Hosten_PKG_Sound.FS_Tests.Jump'
DoubleJumpSound=SoundCue'Hosten_PKG_Sound.FS_Tests.Jump'
FallingDamageLandSound=SoundCue'Hosten_PKG_Sound.FS_Tests.Land'

LandSound=SoundCue'Hosten_PKG_Sound.FS_Tests.Land'

DefaultFootstepSound=SoundCue'Hosten_PKG_Sound.FS_Tests.FS_Metal_Grillage'

FootstepSounds[11]=(MaterialType=Metal_Grillage,Sound=SoundCue'Hosten_PKG_Sound.FS_Tests.FS_Metal_Grillage')
FootstepSounds[12]=(MaterialType=Metal_Taule,Sound=SoundCue'Hosten_PKG_Sound.FS_Tests.FS_Metal_Taule')
FootstepSounds[13]=(MaterialType=Terre,Sound=SoundCue'Hosten_PKG_Sound.FS_Tests.FS_Ground')
FootstepSounds[14]=(MaterialType=Gazon,Sound=SoundCue'Hosten_PKG_Sound.FS_Tests.FS_Grass')
FootstepSounds[15]=(MaterialType=Verre,Sound=SoundCue'Hosten_PKG_Sound.FS_Tests.FS_Metal_Verre')
FootstepSounds[16]=(MaterialType=MatiereMystere,Sound=SoundCue'Hosten_PKG_Sound.FS_Tests.FS_MatiereMystere')


DefaultLandingSound=SoundCue'Hosten_PKG_Sound.FS_Tests.Land'

LandingSounds[16]=(MaterialType=Metal_Grillage,Sound=SoundCue'Hosten_PKG_Sound.FS_Tests.FS_Metal_Grillage')
LandingSounds[18]=(MaterialType=Metal_Taule,Sound=SoundCue'Hosten_PKG_Sound.FS_Tests.FS_Metal_Taule')
LandingSounds[19]=(MaterialType=Terre,Sound=SoundCue'Hosten_PKG_Sound.FS_Tests.FS_Ground')
LandingSounds[20]=(MaterialType=Gazon,Sound=SoundCue'Hosten_PKG_Sound.FS_Tests.FS_Grass')
LandingSounds[21]=(MaterialType=Verre,Sound=SoundCue'Hosten_PKG_Sound.FS_Tests.FS_Metal_Verre')
LandingSounds[22]=(MaterialType=MatiereMystere,Sound=SoundCue'Hosten_PKG_Sound.FS_Tests.FS_MatiereMystere')

}