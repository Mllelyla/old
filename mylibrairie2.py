# encoding: utf-8

from pyo import * 
import random 

#s= Server().boot()

#Beat Machine no 1 

class BM1:
    """Generateur de rythme semi-aleatoire
    explication des arguments:
    accent:
    0 = beat aléatoire, 1 = surtout les temps forts, 2 = les contretemps, 3 = les temps faibles
    dur = le nombre de fois que tu multiplies la longueur du son: 1 = entier, 2 = deux fois plus long...etc
    change = apres combien de mesures le beat change"""
        
    
    def __init__(self,input,time=.125,taps=16,accent=1,dur=1, fx = 'Disto',change=4,mul=.5):
        
        self.table =SndTable(path=input)
        self.time=Sig(time)
        self.dur=Sig(dur)
        self.mul=Sig(mul)
        self.fx = fx
        
        self.taps = taps 
        self.accent = accent 
        
        
        self.change = change
        
        
        inputdur= self.table.getDur()
        
        #accent= Choix des temps presents
        
        self.accent = accent
        w1=0
        w2=0
        w3=0
        if self.accent==0:
            #random beat 
            w1= [random.uniform(0,99)]
            w2= [random.uniform(0,99)]
            w3= [random.uniform(0,99)]
            
        elif self.accent==1:
            #pour les temps forts
            w1=90
            w2=0
            w3=0
            
            
        elif self.accent==2:
            #pour les contretemps
            w1=0
            w2=90
            w3=0
            
        elif self.accent==3:
            #pour les temps faibles
            w1=0
            w2=0
            w3=90
                
        
        
        
        #generation de trigs
        self.metro= Beat(time=self.time,taps=taps,w1=w1,w2=w2,w3=w3,poly=2).play()
        self.amp= TrigEnv(self.metro,table=self.table,dur=inputdur* 1/self.dur, mul=self.metro['amp']*self.mul)
        
                
        self.p1 = SigTo(value=0.5, time=0.05, init=0.5)
        
        self.p2 = SigTo(value=0.5, time=0.05, init=0.5)
        
        self.delay = Delay(self.amp, delay=self.p1, feedback=self.p2, mul=.5).stop()
        
        self.filtre = Biquad(self.amp,freq=self.p1,q=self.p2,type=2,mul=1.5).stop()
        
        self.disto = Disto(self.amp, drive=self.p1, slope=self.p2, mul=.2).stop()
        
        #self.harmo = Harmonizer(self.amp, transpo=Sig(self.p1,mul=24,add=-12),feedback=self.p2).stop()
        
        self.chorus = Chorus(self.amp,depth=self.p1,feedback = self.p2,mix = .5,mul = .2).stop()
        
        self.degrade = Degrade(self.amp, bitdepth=Sig(self.p1,mul=20,add=2),srscale=Sig(self.p2,mul=.99,add=.01), mul=1.5).stop()
        
        self.rien  = self.amp
         
        
         
        self.fx_dict = {'Delay': self.delay, 'Disto': self.disto, 'Degrade': self.degrade, 
                       'Filtre': self.filtre, 'Chorus': self.chorus}
                       
        #self.fx_dict[fx]
       
        
        #Reverb generale 
        self.reverb = Freeverb(input=self.rien, size=.5, damp=.4, bal=.4,mul=.5)
        self.stereo = self.reverb.mix(2) 
        
                
        #change de beat x mesures
        
        self.a = 0
                
        self.nb = TrigFunc(self.metro['end'], self.newbeat)
        
        
    def newbeat(self):
        self.a += 1
        if a >=self.change:
            self.metro.new()
            print 'fin de la section'
            self.a = 0
    
    def out(self):
        self.stereo.out()
        return self
        
    def getOut(self):
        return self.stereo
        
    def setInput(self,x):
        self.table.setSound(x)
        self.amp.dur = self.table.getDur() * 1/self.dur
    
    def setTime(self,x):
        self.time.value= x
        
    def setDur(self,x):
        self.dur.value= x
        
    def getDur(self):
        return self.table.getDur()
        
    def setP1(self,x):
        self.p1.value = x
         
    def setP2(self,x):
        self.p2.value = x 
            
    def setFx(self,fx):
        
        self.fx_dict[self.fx].stop()
        self.fx_dict[fx].out()
        self.reverb.setInput = self.fx_dict[fx]
        self.fx = fx 
        
              
    def setTaps(self,x):
        self.metro.taps = x
        
    def setAccent(self, x ):
        self.accent = x 
        
        if x == 0:
            self.metro.w1= [random.uniform(0,99)]
            self.metro.w2= [random.uniform(0,99)]
            self.metro.w3= [random.uniform(0,99)]
               
        elif x == 1:
            self.metro.w1=90
            self.metro.w2=0
            self.metro.w3=0
            
        elif x == 2:
            self.metro.w1=0
            self.metro.w2=90
            self.metro.w3=0
            
        elif x == 3:
            self.metro.w1=0
            self.metro.w2=0
            self.metro.w3=90
        
       
        
    def setMul(self,x):
        self.mul.value=x 

#player = BM1(input="reyong.wav").out()

#s.gui(locals())

    


    
        
